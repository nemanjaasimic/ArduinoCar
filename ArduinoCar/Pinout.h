// Pinout.h

#ifndef _PINOUT_h
#define _PINOUT_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#pragma region Traction motor setup

#define DRIVE_SPEED 4
#define DRIVE_FORWARD 27
#define DRIVE_BACKWARD 26
#define MAX_ENGINE_RATE 180

#pragma endregion

#pragma region Steering motor setup

#define STEER_SPEED 3
#define STEER_LEFT 24
#define STEER_RIGHT 25
#define MAX_STEERING_RATE 130

#pragma endregion

#pragma region Lights Setup

#define HEAD_LIGHTS 30
#define TAIL_LIGHTS 31
#define TURN_RIGHT_LIGHTS 29
#define TURN_LEFT_LIGHTS 28

#define ON_LIGHT 32
#define CMD_EXEC_LIGHT 33

#pragma endregion

#define HORN_PIN 8
#define HORN_FREQUENCY 380

void initEngine();
void initSteering();
void initLights();
void initHorn();

#endif
