// 
// 
// 

#include "Engine.h"

Engine::Engine()
{
    initEngine();
    engineOn = false;
}

bool Engine::isOn()
{
    return engineOn;
}

bool Engine::turnOn()
{
    if (engineOn)
    {
        Serial.print("\nEngine already on!\n");
        return false;
    }
    else
    {
        Serial.print("\nTurning on the engine!\n");
        digitalWrite(ON_LIGHT, HIGH);
        return engineOn = true;
    }
}

bool Engine::turnOff()
{
    if (!engineOn)
    {
        Serial.print("\nEngine already off!\n");
        return false;
    }
    else
    {
        Serial.print("\nTurning off the engine!\n");
        reset();
        digitalWrite(ON_LIGHT, LOW);
        engineOn = false;
        return true;
    }
}

void Engine::goForward(const short rate)
{
    if (rate < 100 || rate > MAX_ENGINE_RATE || !engineOn)
    {
        Serial.print("\nTurn on the engine or adjust speed rate!\n");
        return;
    }
    else
    {
        Serial.print("\nGoing forward with rate: ");
        Serial.print(rate, DEC);
        Serial.println();
        analogWrite(DRIVE_SPEED, rate);
        digitalWrite(DRIVE_FORWARD, HIGH);
        digitalWrite(DRIVE_BACKWARD, LOW);
    }
}

void Engine::goBackward(const short rate)
{
    if (rate < 100 || rate > MAX_ENGINE_RATE || !engineOn)
    {
        Serial.write("\nTurn on the engine or adjust speed rate!\n");
        return;
    }
    else
    {
        Serial.print("\nGoing backward with rate: ");
        Serial.print(rate, DEC);
        Serial.println();
        analogWrite(DRIVE_SPEED, rate);
        digitalWrite(DRIVE_FORWARD, LOW);
        digitalWrite(DRIVE_BACKWARD, HIGH);
    }
}

void Engine::reset()
{
    if (isOn())
    {
        Serial.print("\nSTOPPING!\n");
        analogWrite(DRIVE_SPEED, 0);
        digitalWrite(DRIVE_FORWARD, LOW);
        digitalWrite(DRIVE_BACKWARD, LOW);
    }
}
