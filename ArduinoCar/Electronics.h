// Electronics.h

#ifndef _ELECTRONICS_h
#define _ELECTRONICS_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include "Pinout.h"
#include "Alarm.h"
#include "pt.h"

static struct pt blinkPt;
static struct pt leftTurnPt;
static struct pt rightTurnPt;
static struct pt hazardPt;

class Electronics
{
private:
    Alarm alarm;
    // true if lights on, false is lights off
    bool lightsOn;
    // true if hazard lights blinking, false if hazard lights off
    bool hazardLightsOn;

    bool leftTurnOn;
    bool rightTurnOn;

public:
    Electronics();
    bool unlock();
    bool lock();
    bool isLocked();
    bool turnLeftOn();
    bool turnRightOn();
    bool hazardOn();
    void turnOnHeadlights();
    void turnOffHeadlights();
    void turnOnBacklights();
    void turnOffBacklights();
    int blinkLights();
    int turnLeftLights();
    void turnLeftLightsOff();
    int turnRightLights();
    void turnRightLightsOff();
    int turnOnHazardLights();
    void turnOffHazardLights();
    void turnOffTurnLights();
    void hornOn();
    void hornOff();
};

#endif