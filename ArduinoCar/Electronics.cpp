// 
// 
// 

#include "Electronics.h"

Electronics::Electronics()
{
    PT_INIT(&hazardPt);
    PT_INIT(&leftTurnPt);
    PT_INIT(&rightTurnPt);

    lightsOn = false;
    hazardLightsOn = false;
    leftTurnOn = false;
    rightTurnOn = false;
    initLights();
    initHorn();
}

bool Electronics::unlock()
{
    return alarm.disarm();
}

bool Electronics::lock()
{
    return alarm.arm();
}

bool Electronics::isLocked()
{
    return alarm.isArmed();
}

bool Electronics::turnLeftOn()
{
    return leftTurnOn;
}

bool Electronics::turnRightOn()
{
    return rightTurnOn;
}

bool Electronics::hazardOn()
{
    return hazardLightsOn;
}

void Electronics::turnOnHeadlights()
{
    digitalWrite(HEAD_LIGHTS, HIGH);
    lightsOn = true;
}

void Electronics::turnOffHeadlights()
{
    digitalWrite(HEAD_LIGHTS, LOW);
    lightsOn = false;
}

void Electronics::turnOnBacklights()
{
    digitalWrite(TAIL_LIGHTS, HIGH);
    lightsOn = true;
}

void Electronics::turnOffBacklights()
{
    digitalWrite(TAIL_LIGHTS, LOW);
    lightsOn = false;
}

int Electronics::blinkLights()
{
    static unsigned long timestamp = 0;
    PT_BEGIN(&blinkPt);
    if (lightsOn)
    {
        digitalWrite(HEAD_LIGHTS, LOW);
        timestamp = millis();
        while (millis() - timestamp < 200);
        digitalWrite(HEAD_LIGHTS, HIGH);
    }
    else
    {
        digitalWrite(HEAD_LIGHTS, HIGH);
        timestamp = millis();
        while (millis() - timestamp < 200);
        digitalWrite(HEAD_LIGHTS, LOW);
    }
    PT_END(&blinkPt);
}

int Electronics::turnLeftLights()
{
    leftTurnOn = true;
    static unsigned long timestamp = 0;
    PT_THREAD();
    PT_BEGIN(&leftTurnPt);
    while (leftTurnOn)
    {
        digitalWrite(TURN_LEFT_LIGHTS, HIGH);
        timestamp = millis();
        PT_WAIT_UNTIL(&leftTurnPt, millis() - timestamp > 500);
        digitalWrite(TURN_LEFT_LIGHTS, LOW);
        timestamp = millis();
        PT_WAIT_UNTIL(&leftTurnPt, millis() - timestamp > 500);
    }
    PT_END(&leftTurnPt);
}

void Electronics::turnLeftLightsOff()
{
    leftTurnOn = false;
    digitalWrite(TURN_LEFT_LIGHTS, LOW);
}

int Electronics::turnRightLights()
{
    rightTurnOn = true;
    static unsigned long timestamp = 0;
    PT_THREAD();
    PT_BEGIN(&rightTurnPt);
    while (rightTurnOn)
    {
        digitalWrite(TURN_RIGHT_LIGHTS, HIGH);
        timestamp = millis();
        PT_WAIT_UNTIL(&rightTurnPt, millis() - timestamp > 500);
        digitalWrite(TURN_RIGHT_LIGHTS, LOW);
        timestamp = millis();
        PT_WAIT_UNTIL(&rightTurnPt, millis() - timestamp > 500);
    }
    PT_END(&rightTurnPt);
}

void Electronics::turnRightLightsOff()
{
    rightTurnOn = false;
    digitalWrite(TURN_RIGHT_LIGHTS, LOW);
}

int Electronics::turnOnHazardLights()
{
    if (rightTurnOn)
        turnRightLightsOff();
    if (leftTurnOn)
        turnLeftLightsOff();

    hazardLightsOn = true;
    static unsigned long timestamp = 0;
    PT_THREAD();
    PT_BEGIN(&hazardPt);
    while (hazardLightsOn)
    {
        digitalWrite(TURN_RIGHT_LIGHTS, HIGH);
        digitalWrite(TURN_LEFT_LIGHTS, HIGH);
        timestamp = millis();
        PT_WAIT_UNTIL(&hazardPt, millis() - timestamp > 500);
        digitalWrite(TURN_RIGHT_LIGHTS, LOW);
        digitalWrite(TURN_LEFT_LIGHTS, LOW);
        timestamp = millis();
        PT_WAIT_UNTIL(&hazardPt, millis() - timestamp > 500);
    }
    PT_END(&hazardPt);
}

void Electronics::turnOffHazardLights()
{
    hazardLightsOn = false;
    digitalWrite(TURN_LEFT_LIGHTS, LOW);
    digitalWrite(TURN_RIGHT_LIGHTS, LOW);
}

    void Electronics::turnOffTurnLights()
{
    digitalWrite(TURN_LEFT_LIGHTS, LOW);
    digitalWrite(TURN_RIGHT_LIGHTS, LOW);
    rightTurnOn = false;
    leftTurnOn = false;
}

void Electronics::hornOn()
{
    tone(HORN_PIN, HORN_FREQUENCY);
}

void Electronics::hornOff()
{
    noTone(HORN_PIN);
}