// 
// 
// 

#include "Car.h"

Car::Car() : engine(), steering(), electronics()
{
}

bool Car::turnOn()
{
    if (electronics.isLocked())
        return false;
    return engine.turnOn();
}

bool Car::turnOff()
{
    if (electronics.isLocked())
        return false;
    return engine.turnOff();
}

bool Car::isOn()
{
    return engine.isOn();
}


bool Car::lockCar()
{
    if (electronics.isLocked() || engine.isOn())
        return false;

    return electronics.lock();
}

bool Car::unlockCar()
{
    return electronics.unlock();
}

bool Car::isLocked()
{
    return electronics.isLocked();
}

bool Car::isLeftOn()
{
    return electronics.turnLeftOn();
}

bool Car::isRightOn()
{
    return electronics.turnRightOn();
}

bool Car::isHazardOn()
{
    return electronics.hazardOn();
}

void Car::turnLeft(const short sensitivity)
{
    steering.steerLeft(sensitivity);
}

void Car::turnRight(const short sensitivity)
{
    steering.steerRight(sensitivity);
}

void Car::goForward(const short rate)
{
    engine.goForward(rate);
}

void Car::goBackward(const short rate)
{
    engine.goBackward(rate);
}

void Car::turnOnLights()
{
    electronics.turnOnHeadlights();
    electronics.turnOnBacklights();

}

void Car::turnOffLights()
{
    electronics.turnOffHeadlights();
    electronics.turnOffBacklights();
}

void Car::blinkLights()
{
    electronics.blinkLights();
}

void Car::leftIndicatorOn()
{
    electronics.turnLeftLights();
}

void Car::leftIndicatorOff()
{
    electronics.turnLeftLightsOff();
}

void Car::rightIndicatorOn()
{
    electronics.turnRightLights();
}

void Car::rightIndicatorOff()
{
    electronics.turnRightLightsOff();
}

void Car::turnOnHazardLights()
{
    electronics.turnOnHazardLights();
}

void Car::turnOffHazardLights()
{
    electronics.turnOffHazardLights();
}

void Car::turnOffTurnLights()
{
    electronics.turnOffTurnLights();
}

void Car::resetSteering()
{
    steering.reset();
}

void Car::hornOn()
{
    electronics.hornOn();
}

// turns off the horn
void Car::hornOff()
{
    electronics.hornOff();
}

void Car::resetMoving()
{
    engine.reset();
}
