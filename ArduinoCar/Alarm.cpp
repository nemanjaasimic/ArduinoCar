// 
// 
// 

#include "Alarm.h"

Alarm::Alarm()
{
    armed = false;
}

bool Alarm::disarm()
{
    for (int i = 0; i < 2; i++)
    {
        digitalWrite(TURN_LEFT_LIGHTS, HIGH);
        digitalWrite(TURN_RIGHT_LIGHTS, HIGH);
        delay(200);
        digitalWrite(TURN_LEFT_LIGHTS, LOW);
        digitalWrite(TURN_RIGHT_LIGHTS, LOW);
        delay(200);

    }
    return armed = false;
}

bool Alarm::arm()
{
    for (int i = 0; i < 2; i++)
    {
        digitalWrite(TURN_LEFT_LIGHTS, HIGH);
        digitalWrite(TURN_RIGHT_LIGHTS, HIGH);
        digitalWrite(HEAD_LIGHTS, HIGH);
        digitalWrite(TAIL_LIGHTS, HIGH);
        tone(HORN_PIN, HORN_FREQUENCY);
        delay(250);
        digitalWrite(TURN_LEFT_LIGHTS, LOW);
        digitalWrite(TURN_RIGHT_LIGHTS, LOW);
        digitalWrite(HEAD_LIGHTS, LOW);
        digitalWrite(TAIL_LIGHTS, LOW);
        noTone(HORN_PIN);
        delay(200);

    }
    return armed = true;
}

bool Alarm::isArmed()
{
    return armed;
}
