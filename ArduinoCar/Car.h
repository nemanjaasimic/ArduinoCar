// Car.h

#ifndef _CAR_h
#define _CAR_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include "Engine.h"
#include "Steering.h"
#include "Electronics.h"

class Car
{
private:
    Engine engine;
    Steering steering;
    Electronics electronics;
public:
    Car();
    // Turns on the car
    bool turnOn();

    // Turns off the car
    bool turnOff();

    bool isOn();

    bool lockCar();

    bool unlockCar();

    bool isLocked();

    bool isLeftOn();

    bool isRightOn();

    bool isHazardOn();

    // Steers left with given sensitivity
    void turnLeft(const short);

    // Steers right with given sensitivity
    void turnRight(const short);

    // Goes forward with given rate
    void goForward(const short);

    // Goes backward with given rate
    void goBackward(const short);

    // turns on headlights and backlights
    void turnOnLights();

    // turns off headlights and backlights
    void turnOffLights();

    // blinks headlights twice
    void blinkLights();

    //
    void leftIndicatorOn();
    
    //
    void leftIndicatorOff();

    //
    void rightIndicatorOn();

    //
    void rightIndicatorOff();

    // turns on all four turn lights
    void turnOnHazardLights();

    // turns off all four turn lgihts
    void turnOffHazardLights();

    void turnOffTurnLights();

    // turns on the horn
    void hornOn();

    // turns off the horn
    void hornOff();

    // resets steering state
    void resetSteering();

    // resets moving state
    void resetMoving();
};

#endif

