// 
// 
// 

#include "Steering.h"
#include "Pinout.h"

Steering::Steering()
{
    initSteering();
}

void Steering::steerLeft(const short sensitivity)
{
    if (sensitivity < 100 || sensitivity > MAX_STEERING_RATE)
    {
        Serial.print("\nAdjust steering sensitivity!\n");
        return;
    }
    Serial.print("\nSteering left with sensitivity: ");
    Serial.print(sensitivity, DEC);
    Serial.println();
    analogWrite(STEER_SPEED, sensitivity);
    digitalWrite(STEER_LEFT, HIGH);
    digitalWrite(STEER_RIGHT, LOW);
}

void Steering::steerRight(const short sensitivity)
{
    if (sensitivity < 100 || sensitivity > MAX_STEERING_RATE)
    {
        Serial.write("\nAdjust steering sensitivity!\n");
        return;
    }

    Serial.print("\nSteering right with sensitivity: ");
    Serial.print(sensitivity, DEC);
    Serial.println();
    analogWrite(STEER_SPEED, sensitivity);
    digitalWrite(STEER_LEFT, LOW);
    digitalWrite(STEER_RIGHT, HIGH);
}

void Steering::reset()
{
    Serial.print("\nWHEELS FORWARD!\n");
    analogWrite(STEER_SPEED, 0);
    digitalWrite(STEER_LEFT, LOW);
    digitalWrite(STEER_RIGHT, LOW);
}
