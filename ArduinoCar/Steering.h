// Steering.h

#ifndef _STEERING_h
#define _STEERING_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include "Pinout.h"

/*
    Module for steering
    Responsible for steering left and right
*/
class Steering
{
public:
    Steering();

    /*
        Steers left, MUST call reset() after calling this method
        params:
            sensitivity - Speed of steering, between 100 and 255
    */
    void steerLeft(const short);

    /*
        Steers right, MUST call reset() after calling this method
        params:
            sensitivity - Speed of steering, between 100 and 255
    */
    void steerRight(const short);

    /*
        Resets steering params, MUST BE CALLED AFTER EVERY CALL TO steerLeft() or steerRight()
    */
    void reset();
};

#endif

