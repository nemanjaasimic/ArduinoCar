// In/struction.h

#ifndef _INSTRUCTION_h
#define _INSTRUCTION_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

enum DIRECTION
{
    NONE_DIR    = 0b0000,
    FORWARD     = 0b0001,
    BACKWARD    = 0b0010,
    RIGHT       = 0b0100,
    LEFT        = 0b1000,

    // masks
    NOT_FORWARD     = 0b1110,
    NOT_BACKWARD    = 0b1101,
    NOT_RIGHT       = 0b1011,
    NOT_LEFT        = 0b0111
};

enum EQUIPMENT
{
    NONE_EQ         = 0b00000000,
    LEFT_INDICATOR  = 0b00000001,
    RIGHT_INDICATOR = 0b00000010,
    TURN_ON         = 0b00000100,
    HEADLIGHTS      = 0b00001000,
    BLINK           = 0b00010000,
    ALARM           = 0b00100000,
    HAZARD_LIGHTS   = 0b01000000,
    HORN            = 0b10000000,

    // masks
    ALL_ON              = 0b11111111,
    LEFT_INDICATOR_OFF  = 0b11111110,
    RIGHT_INDICATOR_OFF = 0b11111101,
    TURN_OFF            = 0b11111011,
    HEADLIGHTS_OFF      = 0b11110111,
    NOT_BLINK           = 0b11101111,
    ALARM_OFF           = 0b11011111,
    HAZARD_LIGHTS_OFF   = 0b10111111,
    HORN_OFF            = 0b01111111
};

#endif