// Engine.h

#ifndef _ENGINE_h
#define _ENGINE_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include "Pinout.h"

/*	Engine module, responsible for turning on/off the engine
    and driving forward and backward
*/
class Engine
{
private:
    // true if engine is on, false is engine is off
    bool engineOn;
public:
    Engine();

    /*
        Returns state of the engine
    */
    bool isOn();

    /*
        Turns on the engine if engine is off, otherwise returns false
    */
    bool turnOn();

    /*
        Turns off the engine if engine is on, otherwise returns false
    */
    bool turnOff();

    /*	Moves car forward, MUST call reset() after calling this method
        params:
            rate - Rate at which engine works, 0-off, 255-max revs
    */
    void goForward(const short);

    /*	Moves car backward, MUST call reset() after calling this method
        params:
            rate - Rate at which engine works, 0-off, 255-max revs
    */
    void goBackward(const short);

    /*
        Resets engine moving state
        MUST CALL AFTER EVERY CALL TO goForward() or goBackward()
    */
    void reset();
};

#endif
