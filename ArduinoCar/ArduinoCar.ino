/*
 Name:		ArduinoCar.ino
 Created:	09 Oct 2017 23:47:01
 Author:	Nemanja Simic
*/

#include "Instruction.h"
#include "Electronics.h"
#include "Steering.h"
#include "Engine.h"
#include "Car.h"
#include "Alarm.h"
#include "Pinout.h"
#include "Instruction.h"
#include "pt.h"

#define BUFFER_SIZE 4
#define COMMAND_IDX 0
#define SPEED_IDX 1
#define STEERING_IDX 2
#define ELECTRONICS_IDX 3
Car car;
byte* buffer = new byte[BUFFER_SIZE];

void setup()
{
    emptyBuffer();
    Serial.begin(9600);
    Serial2.begin(115200);
    // wait for wifi to start up
    Serial.println("\nStarting up WiFi...");
    delay(300);
}

void loop()
{
    if (Serial2.available()) {
        //Serial.println("\nReceived instructions...");
        Serial2.readBytes(buffer, BUFFER_SIZE);
        Serial2.flush();
        //for (int i = 0; i < BUFFER_SIZE; i++) 
        //{
        //	Serial.print(i);
        //	Serial.print(". parametar : ");
        //	Serial.println(buffer[i]);
        //	//delay(200);
        //}
        /*if (buffer[SPEED_IDX] > MAX_ENGINE_RATE) 
        {
            buffer[SPEED_IDX] = buffer[SPEED_IDX] - (buffer[SPEED_IDX] - MAX_ENGINE_RATE);
        }

        if (buffer[STEERING_IDX > MAX_STEERING_RATE])
        {
            buffer[STEERING_IDX] = buffer[STEERING_IDX] - (buffer[STEERING_IDX] - MAX_STEERING_RATE);
        }*/
        
        /*buffer[SPEED_IDX] = (((buffer[SPEED_IDX] - 100) * 50) / 155) + 90;
        buffer[STEERING_IDX] = (((buffer[STEERING_IDX] - 100) * 30) / 155) + 90;*/



        handleInstructions();
    }
}

void handleInstructions()
{
    if ((buffer[ELECTRONICS_IDX] & ALARM) == ALARM)
    {
        if (!car.isOn())
        {
            if (!car.isLocked())
            {
                car.hornOff();
                car.turnOffLights();
                car.lockCar();
            }
            return;
        }
    }
    // here we know the alarm is off
    if (car.isLocked())
        car.unlockCar();

    if ((buffer[ELECTRONICS_IDX] & TURN_ON) == TURN_ON)
    {
        if (!car.isOn())
            car.turnOn();

        if (buffer[COMMAND_IDX] == NONE_DIR)
        {
            // reset
            car.resetMoving();
            car.resetSteering();
            digitalWrite(CMD_EXEC_LIGHT, LOW);
        }
        else
        {
            digitalWrite(CMD_EXEC_LIGHT, HIGH);
            handleDirections();
        }

        handleElectronics();
    }
    else
    {
        if (buffer[ELECTRONICS_IDX] == NONE_EQ)
        {
            // reset
            car.hornOff();
            car.turnOffLights();
        }

        if (car.isOn())
        {
            // if car is on and moving and we turn the engine off
            // just reset its moving state and turn off the engine
            car.resetMoving();
            car.resetSteering();
            digitalWrite(CMD_EXEC_LIGHT, LOW);
            car.turnOff();
        }

        // if car is off we still can turn on/off lights, turn lights, honk the horn etc
        handleElectronics();
    }

    emptyBuffer();
}

void handleDirections()
{
    // disables movement function based on the instruction
    if ((buffer[COMMAND_IDX] | NOT_FORWARD) == NOT_FORWARD
        && (buffer[COMMAND_IDX] | NOT_BACKWARD) == NOT_BACKWARD)
    {
        car.resetMoving();
    }
    // disables steering function based on the instruction
    if ((buffer[COMMAND_IDX] | NOT_LEFT) == NOT_LEFT
        && (buffer[COMMAND_IDX] | NOT_RIGHT) == NOT_RIGHT)
    {
        car.resetSteering();
    }
    // generates movement based on the instruction
    if ((buffer[COMMAND_IDX] & FORWARD) == FORWARD)
        car.goForward(buffer[SPEED_IDX]);

    if ((buffer[COMMAND_IDX] & BACKWARD) == BACKWARD)
        car.goBackward(buffer[SPEED_IDX]);

    if ((buffer[COMMAND_IDX] & RIGHT) == RIGHT)
        car.turnRight(buffer[STEERING_IDX]);

    if ((buffer[COMMAND_IDX] & LEFT) == LEFT)
        car.turnLeft(buffer[STEERING_IDX]);
}

void handleElectronics()
{
    if ((buffer[ELECTRONICS_IDX] | HEADLIGHTS_OFF) == HEADLIGHTS_OFF)
        car.turnOffLights();

    if ((buffer[ELECTRONICS_IDX] | HORN_OFF) == HORN_OFF)
        car.hornOff();

    if ((buffer[ELECTRONICS_IDX] & HEADLIGHTS) == HEADLIGHTS)
        car.turnOnLights();

    if ((buffer[ELECTRONICS_IDX] & HORN) == HORN)
        car.hornOn();

    if ((buffer[ELECTRONICS_IDX] & LEFT_INDICATOR) == LEFT_INDICATOR)
    {
        car.turnOffHazardLights();
        car.leftIndicatorOn();
    }

    if (car.isLeftOn())
    {
        if ((buffer[ELECTRONICS_IDX] | LEFT_INDICATOR_OFF) == LEFT_INDICATOR_OFF)
            car.leftIndicatorOff();
    }

    if ((buffer[ELECTRONICS_IDX] & RIGHT_INDICATOR) == RIGHT_INDICATOR)
    {
        car.turnOffHazardLights();
        car.rightIndicatorOn();
    }

    if (car.isRightOn())
    {
        if ((buffer[ELECTRONICS_IDX] | RIGHT_INDICATOR_OFF) == RIGHT_INDICATOR_OFF)
            car.rightIndicatorOff();
    }

    if ((buffer[ELECTRONICS_IDX] & HAZARD_LIGHTS) == HAZARD_LIGHTS)
    {
        car.turnOffTurnLights();
        car.turnOnHazardLights();
    }

    if (car.isHazardOn())
    {
        if ((buffer[ELECTRONICS_IDX] | HAZARD_LIGHTS_OFF) == HAZARD_LIGHTS_OFF)
            car.turnOffHazardLights();
    }

    if ((buffer[ELECTRONICS_IDX] & BLINK) == BLINK)
        car.blinkLights();
}

void emptyBuffer()
{
    delete[] buffer;
    buffer = new byte[BUFFER_SIZE];
    for (int i = 0; i < BUFFER_SIZE; i++)
    {
        if (i == SPEED_IDX || i == STEERING_IDX)
            buffer[i] = 100;
        else
            buffer[i] = 0;
    }
}
