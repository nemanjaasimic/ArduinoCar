// Alarm.h

#ifndef _ALARM_h
#define _ALARM_h

#if defined(ARDUINO) && ARDUINO >= 100
    #include "Arduino.h"
#else
    #include "WProgram.h"
#endif

#include "Pinout.h"

class Alarm
{
private:
    bool armed;
public:
    Alarm();
    bool disarm();
    bool arm();
    bool isArmed();
};

#endif
