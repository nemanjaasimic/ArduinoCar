// 
// 
// 

#include "Pinout.h"

void initEngine()
{
    pinMode(DRIVE_SPEED, OUTPUT);
    pinMode(DRIVE_FORWARD, OUTPUT);
    pinMode(DRIVE_BACKWARD, OUTPUT);
}

void initSteering()
{
    pinMode(STEER_SPEED, OUTPUT);
    pinMode(STEER_LEFT, OUTPUT);
    pinMode(STEER_RIGHT, OUTPUT);
}

void initLights()
{
    pinMode(HEAD_LIGHTS, OUTPUT);
    pinMode(TAIL_LIGHTS, OUTPUT);
    pinMode(TURN_RIGHT_LIGHTS, OUTPUT);
    pinMode(TURN_LEFT_LIGHTS, OUTPUT);

    pinMode(ON_LIGHT, OUTPUT);
    pinMode(CMD_EXEC_LIGHT, OUTPUT);
}

void initHorn()
{
    pinMode(HORN_PIN, OUTPUT);
}
