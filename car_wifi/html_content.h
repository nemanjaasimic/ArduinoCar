#ifndef _HTML_CONTENT_H
#define _HTML_CONTENT_H


const char ROOT_PAGE[] PROGMEM = R"=====(
<!DOCTYPE HTML>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<title>ArduinoCar Home</title>
		<style>
			html, body { height: 100%; margin: 0; padding: 0; width: 100%; }
			body { display:table; background-color: #5C9BD3; font-family: Arial, Helvetica, Sans-Serif; Color: #FFFFFF; }
			.centered { text-align: center; display: table-cell; vertical-align: middle; }
		</style>
	</head>
	<body>
		<div class="centered">
			<h1>Welcome to ArduinoCar homepage!</h1>
			<h3>Please send your HTTP POST requests to <a href="http://192.168.7.3/car">/car</a> page.</h3>
		</div>
	</body>
</html>
)=====";

const char ERROR_CAR[] = "Invalid request!\nPlease include those parameters into request body:\n* direction \n* speed \n* steering \n* equipment \n";

#endif