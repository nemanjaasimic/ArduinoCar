#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include "html_content.h"

#define AP_NAME "ArduinoCar"
#define AP_PASS "drivemycar"
#define HOSTNAME "arduinocar"
#define PORT 80
#define BUFFER_SIZE 4

ESP8266WebServer server(PORT);
IPAddress ip(192, 168, 7, 3);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
WiFiEventHandler stationDisconnectedHandler;

byte direction;
byte speed;
byte steering;
byte equipment;

void setup() {
  initParams();
  WiFi.persistent(true);
  WiFi.hostname(HOSTNAME);
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(ip, gateway, subnet);
  WiFi.softAP(AP_NAME, AP_PASS);
  stationDisconnectedHandler = WiFi.onSoftAPModeStationDisconnected(&onDisconnect);

  server.on("/", handleRoot);
  server.on("/car", HTTP_POST, handleInstruction);
  server.onNotFound(handleNotFound);
  server.begin();

  // Info
  Serial.begin(115200);
  Serial.print("AP Name is: ");
  Serial.println(AP_NAME);
  Serial.print("AP Pass is: ");
  Serial.println(AP_PASS);
  Serial.print("Server IP is: ");
  Serial.println(WiFi.softAPIP());
  Serial.println("\nWaiting for instructions...");
  Serial.println("##");  
  delay(200);
}

void loop() {
  server.handleClient();
}

void handleInstruction() {
  if(!server.hasArg("direction") || !server.hasArg("equipment")
      || !server.hasArg("speed") || !server.hasArg("steering")) {

    server.send(400, "text/plain", ERROR_CAR);
    return;
  }

  direction = server.arg("direction").toInt();
  speed = server.arg("speed").toInt();
  steering = server.arg("steering").toInt();
  equipment = server.arg("equipment").toInt();

  writeParams();
  server.send(200, "text/plain", "Successfully received.");
}

void handleNotFound() {
  server.send(404, "text/plain", "Request not found.");  
}

void handleRoot() {
  server.send(200, "text/html", ROOT_PAGE);
}

void onDisconnect(const WiFiEventSoftAPModeStationDisconnected& evt) {
  initParams();
  writeParams();
}

void writeParams() {
  byte buffer[] = { direction, speed, steering, equipment };
  Serial.write(buffer, BUFFER_SIZE);
}

void initParams() {
  direction = 0;
  speed = 100;
  steering = 100;
  equipment = 0;
}
